package address_book_system;
import address_book_system.address_bookPackage.*;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import java.io.*;

//
//
// AddressBookClient
//
// This client demonstrates the AddressBook server and servant.
// A menu is presented to the user, allow he or she to add
// users, and look up their names & email addresses
//
//
public class AddressBookClient 
{
    public static void main(String args[]) throws IOException
    {
        try
	{
	    // Create an object request broker
	    ORB orb = ORB.init(args, null);

	    // Obtain object reference for name service ...
            org.omg.CORBA.Object object = 
            orb.resolve_initial_references("NameService");
			
            // ... and narrow it to a NameContext
            NamingContext namingContext = 
            NamingContextHelper.narrow(object);

            // Create a name component array
            NameComponent nc_array[] =
            { new NameComponent("address_book","") };

            // Get an address book object reference ...
            org.omg.CORBA.Object objectReference = 
            namingContext.resolve(nc_array);

            // ... and narrow it to get an address book
            address_book AddressBook = 
            address_bookHelper.narrow(objectReference);

            // DataInputStream for system.in
            DataInputStream din = new DataInputStream 
            (System.in);

            for (;;)
            {
                try
                {
		    // Print menu
                    System.out.println ("1- Add user");
                    System.out.println ("2- Look up email");
                    System.out.println ("3- Look up name");
                    System.out.println ("4- Exit");

                    System.out.print ("Command :");

                    // Read a line from user
                    String command = din.readLine();

                    // Convert to number
                    Integer num = new Integer (command);
                    int choice = num.intValue();

                    // Variables we'll need for service calls
                    String name;
                    String email;

                    switch (choice)
                    {
                        case 1:
			System.out.print ("Name:");
			name  = din.readLine();
			System.out.print ("Email:");
			email = din.readLine();

			// Call AddressBook service
			AddressBook.record_user(name,email);
			break;
			
			case 2:
			System.out.println ("Name:");
			name  = din.readLine();

			// Call AddressBook service
			email = AddressBook.email_from_name(name);
			System.out.println ("Email of " + name + " is " + email);
			break;
			
			case 3:
			System.out.println ("Email:");
			email  = din.readLine();

        		// Call AddressBook service
			name = AddressBook.name_from_email(email);
			System.out.println ("Name of " + email + " is " + name);		
			break;
	
			case 4:
			System.exit(0);
                    }
                }
		catch (user_exists  already_there)
		{
		    System.out.println ("User already exists - cannot be added to address book");
		}
		catch (unknown_user bad_user)
		{
		    System.out.println ("User doesn't exist");
		}
            }
        }
	catch (Exception e)
	{
            System.err.println ("CORBA error - " + e);
	}
    }
}