package address_book_system;
import  address_book_system.address_bookPackage.*;

import  java.util.Hashtable;
import  java.util.Enumeration;
//
//
// AddressBookServant
//
// This servant class is responsible for implementing
// three methods
//
//   * String name_from_email ( String email );
//   * String email_from_name ( String name  );
//   * void   record_user ( String name, String email ); 
//
//
class AddressBookServant extends _address_bookImplBase
{
    private Hashtable name2email;

    public AddressBookServant()
    {
	// Create a new hashtable to store name & email
	name2email = new Hashtable();
    }

    // Get the name of this email user
    public String name_from_email ( String email )
           throws unknown_user
    {
	if (name2email.contains(email))
	{
 	    // Begin search for that name
	    for (Enumeration e = name2email.keys(); 
                 e.hasMoreElements();)
	    {
		String name = (String) e.nextElement();
		String e_mail = (String) name2email.get(name);

		// Match on email ?
		if (email.compareTo(e_mail) == 0)
		{
			return name;
		}
	    }
	}

	// User not found - throw unknown user exception
	throw new unknown_user();
    }

    // Get the email of this person
    public String email_from_name ( String name  ) 
           throws unknown_user
    {
	// If user exists
	if (name2email.containsKey(name))
	{
		// Return email address
		return (String) name2email.get(name);
	}

	// User doesn't exist
	throw new unknown_user();
    }

    // Add a new user to the system
    public void record_user ( String name, String email )
           throws user_exists
    {
	// Is the user already listed
	if (name2email.containsKey( name ) ||
	    name2email.contains( email ) )
	{
		// If so, throw exception
		throw new user_exists();
	}

	// Add to our hash table
	name2email.put (name, email);
    }
}