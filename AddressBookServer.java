package address_book_system;
import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;

//
//
// AddressBookServer
//
// This server is responsible for creating an object
// request broker (ORB), and registering an AddressBookServant
// with a naming service
//
//
public class AddressBookServer 
{
    public static void main(String args[])
    {
	try
	{
		// Create a new object request broker
		ORB orb = ORB.init(args, null);

		// Create a new address book ...
		AddressBookServant servant = new AddressBookServant();

		// ... and connect it to our orb
		orb.connect(servant);

		// Object Request Broker Initialised
		System.out.println ("Address book ORB initialised");

		// Obtain reference for our nameservice
		org.omg.CORBA.Object object = 
		orb.resolve_initial_references("NameService");

		// Since we have only an object reference, we must
		// cast it to a NamingContext. We use a helper
		// class for this purpose
		NamingContext namingContext = 
		NamingContextHelper.narrow(object);

		// Add a new naming component for our interface
		NameComponent list[] = 
		{ new NameComponent("address_book", "") };

		// Now notify naming service of our new interface
		namingContext.rebind(list, servant);

		// Wait for clients
		for (;;){}
	}
	catch (Exception e)
	{
		System.err.println ("ORB Error - " + e);
		e.printStackTrace(System.out);
		System.exit(0);
	}
    }
}